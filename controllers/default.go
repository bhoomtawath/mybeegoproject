package controllers

import (
	"github.com/astaxie/beego"
)

type MainController struct {
	beego.Controller
}

// @router / [get]
func (c *MainController) Get() {
	c.Data["Website"] = "beego.me"
	c.Data["Email"] = "astaxie@gmail.com"
	c.TplNames = "index.tpl"
}

// @router /getEnvironment [get]
func (c *MainController) GetEnvironment() {
	c.Data["json"] = map[string]string{"env": "dev"}
	c.ServeJson()
}