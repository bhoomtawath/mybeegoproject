package controllers
import (
	"github.com/astaxie/beego"
	"encoding/json"
	//"log"
)

type LoginController struct {
	beego.Controller
}

func (this *LoginController) RenderLoginForm() {
	this.Layout = "layout.tpl"
	this.LayoutSections = make(map[string]string)
	this.LayoutSections["Header"] = "header.tpl"
	this.TplNames = "login.tpl"
}

//Read post body
func (this *LoginController) Login() {
	type Login struct{
		Email string `json:"email"`
		Password string `json:"password"`
	}

	var loginObj Login

	json.Unmarshal(this.Ctx.Input.RequestBody, &loginObj)

	this.Data["json"] = loginObj
	this.ServeJson()
}

//Read form input
func (this *LoginController) PostLoginForm() {
	type Login struct{
		Email string
		Password string
	}

	email := this.GetString("email")
	this.Data["json"] = map[string]string{"email": email}
	this.ServeJson()
}