package main

import (
	"github.com/astaxie/beego"
	_ "gitlab.com/varshard/mybeegoproject/routers"
)

func main() {
	beego.Run()
}
