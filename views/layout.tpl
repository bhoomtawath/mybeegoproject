<!DOCTYPE html>
<html>
  <head></head>
  <body>
    {{.Header}}
    <section class="container">
      {{.LayoutContent}}
    </section>
  </body>
</html>