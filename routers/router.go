package routers

import (
	"gitlab.com/varshard/mybeegoproject/controllers"
	"github.com/astaxie/beego"
)

func init() {
	beego.Include(&controllers.MainController{})
    //beego.Router("/", &controllers.MainController{})
	apiNameSpace := beego.NewNamespace("/api",
		beego.NSRouter("/getEnvironment", &controllers.MainController{}, "get:GetEnvironment"))

	beego.AddNamespace(apiNameSpace)
	beego.Router("/login", &controllers.LoginController{}, "get:RenderLoginForm;post:Login")
	beego.Router("/signIn", &controllers.LoginController{}, "post:PostLoginForm")
}
