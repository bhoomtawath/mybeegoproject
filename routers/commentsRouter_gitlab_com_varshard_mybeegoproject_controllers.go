package routers

import (
	"github.com/astaxie/beego"
)

func init() {

	beego.GlobalControllerRouter["gitlab.com/varshard/mybeegoproject/controllers:MainController"] = append(beego.GlobalControllerRouter["gitlab.com/varshard/mybeegoproject/controllers:MainController"],
		beego.ControllerComments{
			"Get",
			`/`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["gitlab.com/varshard/mybeegoproject/controllers:MainController"] = append(beego.GlobalControllerRouter["gitlab.com/varshard/mybeegoproject/controllers:MainController"],
		beego.ControllerComments{
			"GetEnvironment",
			`/getEnvironment`,
			[]string{"get"},
			nil})

}
